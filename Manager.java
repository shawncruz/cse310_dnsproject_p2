import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

class Manager implements Runnable {

	Socket socket;
	static Map<String,Integer> portMap = new HashMap<>();

    Manager(Socket socket) {
       this.socket = socket;
    }

	public static void main(String args[]) throws Exception {

		ServerSocket welcomeSocket = new ServerSocket(0);

		print("Manager: " + Constant.PORT + welcomeSocket.getLocalPort());
		print(Constant.MANAGER_START);
		print("\nListening");
	
		while (true) {
			Socket sock = welcomeSocket.accept();
			System.out.println("Connected");
			new Thread(new Manager(sock)).start();
		}
		//welcomeSocket.close();
	}

	public void run() {

		String line="",log="";
		boolean running = true;
		List<String> types = read();

		if(portMap.isEmpty())
			portMap = startProcs(types);
		
		String resultString = "";

		try {
			
			BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
			print("before while");
			//line = fromClient.readLine();
			//toClient.writeBytes("FIRST MESSAGE" + "\n");
			
			while(running && (line = fromClient.readLine()) != null) {

				print("STILL IN WHILE");

				String command = line.split(Constant.SPACE)[0].toUpperCase();
	            print("Client sent: " + line);
	            print("Command: " + command + Constant.NL);

	            log = log + line;

	            int pnum;

	            switch(command) {

                case "@START":  resultString = "~WELCOME TO MANAGER!";
                                break;

                case "HELP":    resultString = "HELP MENU MANAGER!";
				                break;

                case "TYPE":    String type = line.split(Constant.SPACE)[1].toUpperCase();

                				if(portMap.containsKey(type)) {
                					print("CONTAINS THIS TYPE");
                					pnum = portMap.get(type);
									resultString = type + "," + pnum;
									running = false;
                				}
                				else {
                					print("DOESNT CONTAINS THIS TYPE");
                					resultString = "This is an invalid type!";
                				}

                				/*pnum = portMap.get(type);
								resultString = type + "," + pnum;
								running = false;*/

                                break;

                case "EXIT":    resultString = line;
                                break;

                default:        resultString = "INVALID COMMAND MANAGER!";
                                break;
            	}

	            toClient.writeBytes(resultString + "\n");
	            print("SERVER->CLIENT: " + resultString);
			}
			print("OUT OF WHILE");
			socket.close();
			toClient.close();
		}
		catch (IOException e) {
			System.out.println(e);
		}
	}

	/*public void run() {

		String line="",log="";

		try {

			BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());

			while((line = fromClient.readLine()) != null) {

				String command = line.split(Constant.SPACE)[0].toUpperCase();
				print("Client sent: " + line);
				print("Command: " + command + Constant.NL);

				String resultString = "";

				// Append to string to log client history
				log = log + line;

				switch(command) {

					case "@START":  resultString = "~WELCOME TO DNS APP!";
					break;

					case "HELP":    resultString = "HELP MENU SERVER!";
					break;

					case "PUT":     resultString = ServerHelper.put(line);
					break;

					case "GET":     resultString = ServerHelper.get(line);
					break;

					case "DEL":     resultString = ServerHelper.del(line);
					break;

					case "BROWSE":  resultString = ServerHelper.browse();
					break;

					case "EXIT":    resultString = line;
					break;

					default:        resultString = "INVALID COMMAND SERVER!";
					break;
				}

				toClient.writeBytes(resultString + "\n");
				print("SERVER->CLIENT: " + resultString);

			}

			// Display log history of client
			print("Log:" + log);

			socket.close();
			toClient.close();
		}
		catch (IOException e) {
			System.out.println(e);
		}
    }*/

    public static Map<String,Integer> startProcs(List<String> types) {

	    Map<String,Integer> portMap = new HashMap<>();
    	try {            
	            Runtime rt = Runtime.getRuntime();
	            Process proc = rt.exec("javac Server.java");	

	            InputStream is = proc.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);

				String line = "";
	 
				while ((line = br.readLine()) != null) {
					System.out.println(line);
				}

				int exitVal = proc.waitFor();
	        	System.out.println("Process exitValue: " + exitVal);
	        	print("number of types: " + types.size());

	            for(String type: types) {
	            	print("type: " + type);
		            proc = rt.exec("java Server " + type);

		            is = proc.getInputStream();
					isr = new InputStreamReader(is);
					br = new BufferedReader(isr);
					line = "";
		 			boolean foundPort = false;
					while (!foundPort && (line = br.readLine()) != null) {
						print("top while");
						if(line.contains("Number:")) {
							print("inside if");
							Pattern pattern = Pattern.compile("\\[([^\\]]+)]");
							Matcher matcher = pattern.matcher(line);
				            if(matcher.find()){
				            	int portNumber = Integer.parseInt(matcher.group(1));
				              	print("type: " + type);
				                print("port number: " + portNumber);
				                String key = type;
				                int value = portNumber;
				                portMap.put(key, value);
				            }
				            foundPort = true;
						}

						System.out.println(line);
						
					}

					//exitVal = proc.waitFor();
		        	print("Process exitValue: " + exitVal);
		        }

		        is.close();
		        isr.close();
		        br.close();
		        

        } catch (Throwable t) {
            t.printStackTrace();
        }
        return portMap;
    }

	public static List<String> read() {

        String fileName = "manager.in";
        String line = ""; // A line in a file
        List<String> typeList = new ArrayList<>();
        
        try {

	        FileReader fileReader = new FileReader(fileName);
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        
	        while((line = bufferedReader.readLine()) != null) {
	            //System.out.println(line);
	            typeList.add(line);
	        }   

	        bufferedReader.close();         
        }
        catch(IOException e) {
            e.printStackTrace();              
        }
        finally {
        	return typeList;
        }
	}

	private static void print(Object o) {
	  	System.out.println(o);
	}

}