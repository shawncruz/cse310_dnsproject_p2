import java.io.*;
import java.net.*;
import java.util.*;

/**
* For Linux machine:
*   Login: shcruz
*   Password: zsFE15CS
*/
public class Server implements Runnable {

   Socket socket;
   String type;
   static boolean started = false;
   static String fileName = "";

   Server(Socket socket, String type) {
      this.socket = socket;
      this.type = type;
   }

  public static void main(String args[]) throws Exception {

      //print(Constant.SERVER_START);
      //System.out.println("Listening");
      ServerSocket welcomeSocket = null;
      String type = "";

      // Check if server is started
      if(!started) {
        welcomeSocket = start();
        print(Constant.PORT + "[" + welcomeSocket.getLocalPort() + "]" + "\n");
        // Create new file if doesn't already exist
        type = args[0];

        write("Server Type: "  + type);

        fileName = "db_type_" + type + ".txt";
        make(fileName);
        started = true;
      }

      /*print("arg list:");
      for(int i = 0; i < args.length; i++)
        print("arg: " + args[i]);*/

      while (true) {
         Socket sock = welcomeSocket.accept();
         System.out.println("Connected");
         new Thread(new Server(sock,type)).start();
      }
  }

  public void run() {

      String line="",log="";

      try {

        BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
        
        while((line = fromClient.readLine()) != null) {
          
            String command = line.split(Constant.SPACE)[0].toUpperCase();
            write("client sent: " + line + " command: " + command);
            print("Client sent: " + line);
            print("Command: " + command + Constant.NL);

            String resultString = "";

            // Append to string to log client history
            log = log + line;

            switch(command) {

                case "@START":  resultString = "~WELCOME TO TYPE " + type + " SERVER";
                                break;

                case "HELP":    resultString = "HELP MENU SERVER!";
                                break;

                case "PUT":     write("YOU DID A PUT");
                                resultString = ServerHelper.put(line, fileName);
                                break;

                case "GET":     write("YOU DID A GET");
                                resultString = ServerHelper.get(line, fileName);
                                break;

                case "DEL":     print("YOU DID A DEL");
                                resultString = ServerHelper.del(line, fileName);
                                break;

                case "BROWSE":  print("YOU DID A BROWSE");
                                resultString = ServerHelper.browse(fileName);
                                break;

                case "DONE":    resultString = "DONE COMMAND FROM SERVER";
                                break;

                default:        resultString = "INVALID COMMAND: TYPE "+ type +" SERVER!";
                                break;
            }

            toClient.writeBytes(resultString + "\n");
            print("SERVER->CLIENT: " + resultString);

        }

        // Display log history of client
        print("Log:" + log);
        
        socket.close();
        toClient.close();
      }
      catch (IOException e) {
         System.out.println(e);
      }
  }

  public static ServerSocket start() throws Exception {

      ServerSocket welcomeSocket = new ServerSocket(0);
      return welcomeSocket;
      
  }

  public static void make(String fileName) throws Exception {

      File file = new File(fileName);
      if(!file.exists()) {
        print("true");
        file.createNewFile();
      }
  }

  public static void write(String message) {

      String fileName = "log.txt";

      try {
            BufferedWriter bufferedWriter = new BufferedWriter(
              new FileWriter(fileName, true));

            bufferedWriter.write(message);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
  }

  private static void print(Object o) {
      System.out.println(o);
  }

}