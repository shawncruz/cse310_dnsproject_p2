import java.io.*;
import java.net.*;

public class Client {

    static int managerPort;
    static String host;

    public static void main(String[] args) throws Exception {

        host = args[0];
        managerPort = Integer.parseInt(args[1]);

        connectToManager();
        
    }

    public static void connectToServer(String type, String hostName, int serverPort) throws Exception {


        print("MADE IT!!");
        print("type: " + type);
        print("hostName: " + hostName);
        print("serverPort: " + serverPort);

        Socket clientSocket = new Socket(hostName, serverPort);
        DataOutputStream toServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        String responseLine;
        String inputString = "";
        System.out.println("The client started. Type any text. To quit it type 'exit'.");
  
        // Prompt user for input
        print(Constant.PROMPT_USER);

        // TAKE THIS OUT WHEN WORKING ON SERVER SIDE FILE
        toServer.writeBytes("@START" + "\n");

        while ((responseLine = fromServer.readLine()) != null 
          && !responseLine.equalsIgnoreCase("EXIT")) {

            // GET RID OF > CHARACTER; MAKE MORE READABLE
            print(">" + responseLine.replaceAll("\\@", "\n") + "\n");

            inputString = userInput.readLine();

            String[] arguments = inputString.split(Constant.SPACE);
            String command = arguments[0].toUpperCase();
          
            switch(command) {

              case "HELP":    print("HELP CASE!");
                              print("\n" + Constant.SERVER_MENU);
                              toServer.writeBytes(command + Constant.NL);
                              break;

              case "PUT":     print("PUT CASE!");
                              toServer.writeBytes(inputString + Constant.NL);
                              break;

              case "GET":     print("GET CASE!");
                              toServer.writeBytes(inputString + Constant.NL);
                              break;

              case "DEL":     print("DEL CASE!");
                              toServer.writeBytes(inputString + Constant.NL);
                              break;

              case "BROWSE":  print("BROWSE CASE!");
                              toServer.writeBytes(inputString + Constant.NL);
                              break;

              case "DONE":    print("DONE CASE!");
                              toServer.writeBytes(command + Constant.NL);
                              responseLine = fromServer.readLine();
                              toServer.close();
                              fromServer.close();
                              //userInput.close();
                              clientSocket.close();
                              connectToManager();
                              continue;

              default:        toServer.writeBytes(command + Constant.NL);
                              print("error: invalid command");
                              break;
            }
            //toServer.writeBytes(command + "\n");
        }
    }

    public static void connectToManager() throws Exception {


        print("MANAGER!!");
        print("hostName: " + host);
        print("serverPort: " + managerPort);

        Socket clientSocket = new Socket(host, managerPort);
        DataOutputStream toManager = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader fromManager = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        String responseLine;
        String inputString = "";
        System.out.println("The client started. Type any text. To quit it type 'exit'.");
  
        // Prompt user for input
        print(Constant.PROMPT_USER);

        // TAKE THIS OUT WHEN WORKING ON SERVER SIDE FILE
        toManager.writeBytes("@START" + "\n");

        while ((responseLine = fromManager.readLine()) != null 
          && !responseLine.equalsIgnoreCase("EXIT")) {

            print(">" + responseLine.replaceAll("\\@", "\n") + "\n");

            inputString = userInput.readLine();

            String[] arguments = inputString.split(Constant.SPACE);
            String command = arguments[0].toUpperCase();
          
            switch(command) {

              case "HELP":    print("HELP CASE!");
                              print("\n" + Constant.MANAGER_MENU);
                              toManager.writeBytes(command + Constant.NL);
                              break;

              case "TYPE":    toManager.writeBytes(inputString + Constant.NL);
                              responseLine = fromManager.readLine();
                              if(responseLine.contains("invalid")) {
                                print("\nInvalid Type!\n");
                                toManager.writeBytes("@START" + "\n");
                              } else {
                              String type = responseLine.split("\\,")[0];
                              int port = Integer.parseInt(responseLine.split("\\,")[1]);
                              toManager.close();
                              fromManager.close();
                              //userInput.close();
                              clientSocket.close();
                              connectToServer(type, host, port);
                              }
                              break;

              case "EXIT":    print("EXIT CASE!");
                              toManager.writeBytes(command + Constant.NL);
                              continue;

              default:        toManager.writeBytes(command + Constant.NL);
                              print("error: invalid command");
                                  break;
            }
            //toServer.writeBytes(command + "\n");
        }
        print("AFTER");
        exit(toManager, fromManager, userInput, clientSocket);

    }

    public static void exit(DataOutputStream toServer, BufferedReader fromServer, BufferedReader userInput, Socket clientSocket) throws Exception {
        toServer.close();
        fromServer.close();
        userInput.close();
        clientSocket.close();
        System.exit(1); 
    }

    public static void print(Object o) {
        System.out.println(o);
    }

}
