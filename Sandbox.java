import java.io.*;
import java.net.*;
import java.util.*;

/**
* Sandbox: this class is for testing
*/
class Sandbox {

	public static void main(String argv[]) throws Exception {

		String sentence; 
        String modifiedSentence; 

        String host = argv[0];
        int port = Integer.parseInt(argv[1]);

        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        Socket clientSocket = new Socket(host, port);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream()); 
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // read a line form the standard input
        sentence = userInput.readLine();

        // send the sentence read to the server
        outToServer.writeBytes(sentence + '\n');

        // get the reply from the server
        modifiedSentence = inFromServer.readLine();

        // print the returned sentence
        System.out.println("FROM SERVER: " + modifiedSentence);

        // close the socket
        clientSocket.close();
	}

	public static void make(String fileName) throws Exception {

		File file = new File(fileName);
		if(!file.exists()) {
			print("true");
			file.createNewFile();
		}
	}

	public static String read(String fileName) throws Exception {

        String row = ""; // Refer to string as row like a proper DB
        StringBuilder result = new StringBuilder();

        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        
        while((row = bufferedReader.readLine()) != null) {
            System.out.println(row);
            result.append(row + Constant.NL);
        }

        bufferedReader.close();      

    	return result.toString();
	}

	public static void write(String fileName, String content) throws Exception {

	    FileWriter fileWriter = new FileWriter(fileName);
	    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		bufferedWriter.write(content + Constant.NL);
	    bufferedWriter.close();
	}

	private static void print(Object s) {
		System.out.println(s);
	}

}