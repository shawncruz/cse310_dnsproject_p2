class Constant {
	public static final String ERR = "error";
	public static final String DB = "db.txt";
	public static final String L_BRAK = "<";
	public static final String R_BRAK = ">";
	public static final String COMMA = ",";
	public static final String NL = "\n";
	public static final String BORDER = NL + NL;
	public static final String SPACE = "\\s";
	public static final String RE_COMMA = "\\,";
	public static final String MENU = "HELP: This command takes no argument. It prints a list of supported commands with a brief description for each." + BORDER
									 + "PUT [-name] [-value] [-type]: This command is used to add name records to the name service database. It takes three arguments: name, value, and type, in that order; they constitute the three fields of a name record. After receiving this name record information, the client sends this name record to the server, which enters the record to its name service database. If a record of the same name and type already exists, update the record with the new value." + BORDER
									 + "GET [-name] [-type]: This command is used to send a query to the server database. It takes two arguments: name and type, in that order. Upon receiving a query, if a record with the name and type is found, the server returns the record value to the client. Otherwise, it returns a “not found” error message." + BORDER
									 + "DEL [-name] [-type]:  This command is used to remove a name record from the database. It takes two arguments: name and type, in that order. Upon receiving a query, if a record with the name and type is found, the server removes the record and sends a positive feedback. Otherwise, it returns a “not found” error message." + BORDER
									 + "BROWSE: This command takes no argument. This command is used to retrieve all current name records in the database. Upon receiving a browse request, the server returns the name and type fields of all records, the value field is not included. If the database is empty, the server returns a “database is empty” error message." + BORDER
									 + "EXIT: This command takes no argument. Upon receiving this command, the client terminates the current TCP connection with the server, and the client program exits." + BORDER;
	
	public static final String SERVER_MENU = "HELP: This command takes no argument. It prints a list of supported commands with a brief description for each." + BORDER
									 + "PUT [-name] [-value]: This command is used to add name records to the name service database. It takes two arguments: name and value in that order; they constitute the two fields of a name record. After receiving this name record information, the client sends this name record to the server, which enters the record to its name service database. If a record of the same name already exists, update the record with the new value." + BORDER
									 + "GET [-name]: This command is used to send a query to the server database. It takes one argument: name. Upon receiving a query, if a record with the name is found, the server returns the record value to the client. Otherwise, it returns a “not found” error message." + BORDER
									 + "DEL [-name]:  This command is used to remove a name record from the database. It takes one argument: name. Upon receiving a query, if a record with the name is found, the server removes the record and sends a positive feedback. Otherwise, it returns a “not found” error message." + BORDER
									 + "BROWSE: This command takes no argument. This command is used to retrieve all current name records in the database. Upon receiving a browse request, the server returns the name field of all records, the value field is not included. If the database is empty, the server returns a 'database is empty' error message." + BORDER
									 + "DONE: This command takes no argument. Upon receiving this command, the client terminates the current TCP connection with the server, and the client program reconnects to the Manager." + BORDER;

	public static final String MANAGER_MENU = "HELP: This command takes no argument. It prints a list of supported commands with a brief description for each." + BORDER
									 + "TYPE [-type]: This command is used to connect the client to the specified type server. It takes one arguments: type (e.g.: A,MX,NS); it constitutes the particular server to connect to. After receiving this type information, the manager links the client to the specified server. If the type does not exist the manager return a 'type does not exist' error message." + BORDER
									 + "EXIT: This command takes no argument. Upon receiving this command, the client terminates the current TCP connection with the server, and the client program exits." + BORDER;
	
	public static final String SERVER_START = "Server running" + NL
												+ "Waiting for connection...";
	public static final String MANAGER_START = "Manager running" + NL
												+ "Waiting for connection...";
	public static final String WELCOME = BORDER + " ####################" + NL + "~#``WELCOME TO DN$``#~" + NL + " ####################";
	public static final String PROMPT_USER = "Enter a command:";
	public static final String PORT = BORDER + "Port Number: ";
}